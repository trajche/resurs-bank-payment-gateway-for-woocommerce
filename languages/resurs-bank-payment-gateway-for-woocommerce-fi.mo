��          �            h     i  T   {  F   �  b     1   z     �     �     �     �     �     �  $   �  `         x  �  �     N  Q   b  M   �  M     '   P     x     �     �     �     �     �      �  X   �     I                	                   
                                             Ange personnummer Bestämmer ifall en betalning ska annulleras ifall Resurs Bank API returnerar FROZEN Bestämmer ifall en betalning ska debiteras direkt vid ett lyckat köp Bestämmer ifall pluginet ska vänta på att bedrägerikontrollen körs vid bookPayment eller inte Enter your government id (social security number) Företag Government ID Government id Personnummer Privat Servermiljö Uppdatera tillgängliga betalmetoder Utökar stödet för AftershopFlow (Tillåter bland annat delkrediteringar och delannulleringar) Utökat stöd för AfterShopFlow Project-Id-Version: Resurs Bank Payment Gateway for WooCommerce
PO-Revision-Date: 2019-12-03 15:24+0200
Language-Team: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 2.2.4
X-Poedit-Basepath: ..
X-Poedit-WPHeader: resursbankgateway.php
Plural-Forms: nplurals=2; plural=(n != 1);
X-Poedit-SourceCharset: UTF-8
X-Poedit-KeywordsList: __;_e;_n:1,2;_x:1,2c;_ex:1,2c;_nx:4c,1,2;esc_attr__;esc_attr_e;esc_attr_x:1,2c;esc_html__;esc_html_e;esc_html_x:1,2c;_n_noop:1,2;_nx_noop:3c,1,2;__ngettext_noop:1,2
Last-Translator: TJ Trajche Kralev <trajche@kralev.eu>
Language: fi_FI
X-Poedit-SearchPath-0: .
X-Poedit-SearchPathExcluded-0: *.js
 Enter government ID Decides if a payment should get anulled when Resurs Bank API returns state FROZEN Decides if a payment should be debited immediately after a successful booking Decides if the plugin should wait for the fraud control to run at bookPayment Syötä henkilötunnuksesi (ppkkvvxxxx) Company Henkilötunnus Henkilötunnus Henkilötunnus Private Chosen server environment Update available payment methods Extending the support for AfterShopFlow (Allows partial credits and partial annullments) Extended AfterShopFlow support 